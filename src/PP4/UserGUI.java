//********************************************************************* 
//*                                                                   * 
//*   CIS611             Spring 2018 Luis Castro and  Rocco Meconi    * 
//*                                                                   * 
//*                      Program Assignment PP4                       * 
//*                                                                   * 
//*                      Class Description                            * 
//*                      Renders the UI for the program               *
//*                      Date Created    04/14/18                     * 
//*                                                                   * 
//*                      Saved in: UserGUI.java                       * 
//*                                                                   * 
//*********************************************************************
package PP4;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class UserGUI extends JPanel {

	private JButton scrapeButton;
	private JButton closeButton;
	// add more UI components as needed
	private Scraper scraper;
	private JTextArea textArea;
	private JScrollPane jp;
	HashMap<String, String> urlHash = new HashMap<>();
	private String url = "http://www.nfl.com/players/search?category=position&playerType=current&conferen%20ceAbbr=null&d-447263-p=1&conference=ALL&filter=defensiveback";

	public UserGUI() {

		scraper = new Scraper(url);

		initGUI();
		doTheLayout();

		scrapeButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try { 
					scrape();
				} catch(Exception ex) {
					System.out.println("Error Ocurred" + ex.getMessage());
				}
			}
		});

		closeButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});

	} // end of constructor

	// Creates and initialize the GUI objects
	private void initGUI() {
		textArea = new JTextArea(8, 55);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrapeButton = new JButton("Scrape NFL Page");
		closeButton = new JButton("Close");
		jp = new JScrollPane(textArea);

	}// end of creating objects method

	// Layouts the UI components as shown in the project document
	private void doTheLayout() {
		JPanel top = new JPanel();
		JPanel center = new JPanel();
		JPanel bottom = new JPanel();

		top.setLayout(new FlowLayout());
		top.add(scrapeButton);

		textArea.setLayout(new GridLayout(30,30));
		center.setLayout(new GridLayout());
		center.add(jp);
		
		bottom.setLayout(new FlowLayout());
		bottom.add(closeButton);

		setLayout(new GridLayout(3, 0));
		add(top);
		add(center);
		add(bottom);
	}// end of Layout method

	// Uses the Scraper object reference to return and display the data as shown in
	// the project document
	void scrape() throws Exception {
		String globalDisplayText = "POS     NUM           NAME                             STATUS     TCKL         SCK       INTT       TEAM   \n"  
				+ "=====================================================================================================  \n";
		scraper.parseData();
		ArrayList<HashMap<String, String>> scrappedPlayerStatsList  = scraper.display();
		for (int i = 0; i < scrappedPlayerStatsList.size(); i++) {
			HashMap<String, String> scrappedHash = scrappedPlayerStatsList.get(i);
			String displayText = scrappedHash.get("POS") + "     " 
			+ scrappedHash.get("NUM") + "      "
			+ scrappedHash.get("PLAYER_NAME") + "    " 
			+ scrappedHash.get("STATUS").trim() + "        "
			+ scrappedHash.get("TCKL") + "          " 
			+ scrappedHash.get("SCK").trim() + "          " 
			+ scrappedHash.get("INTT").trim() + "       "
			+ scrappedHash.get("TEAM").trim() + "\n";
			
			globalDisplayText = globalDisplayText + displayText;
		}
		textArea.setText(globalDisplayText);
		
		 try {
	            String str = "SomeMoreTextIsHere";
	            File newTextFile = new File("NFLStat.txt");

	            FileWriter fw = new FileWriter(newTextFile,true);
	            BufferedWriter bw = new BufferedWriter(fw);
	            
	            bw.newLine();
	            bw.write(globalDisplayText);
	            bw.close();

	        } catch (IOException iox) {
	            //do stuff with exception
	            iox.printStackTrace();
	        }
		
	}// end of scrape action event method

	void close() {
		System.exit(0);
	}// end of close action event method

	public static void main(String[] args) {
		JFrame f = new JFrame("NFL Stats");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container contentPane = f.getContentPane();
		contentPane.add(new UserGUI());
		f.pack();
		f.setVisible(true);
	}// end of main method

}// end of class
