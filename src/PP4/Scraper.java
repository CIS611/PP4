//********************************************************************* 
//*                                                                   * 
//*   CIS611             Spring 2018 Luis Castro and  Rocco Meconi    * 
//*                                                                   * 
//*                      Program Assignment PP4                       * 
//*                                                                   * 
//*                      Class Description                            * 
//*                      Process the regular expression patterns      *
//*                      based on the source data input               *
//*                      Date Created    04/14/18                     * 
//*                                                                   * 
//*                      Saved in: Scraper.java                       * 
//*                                                                   * 
//*********************************************************************
package PP4;

import java.awt.List;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.JTextArea;

public class Scraper {

	private Matcher matcher;
	private Regex regex;
	private String url;
	private String display;

	
	private ArrayList<ArrayList<String>> playerStatsList = new ArrayList<ArrayList<String>>();
	private ArrayList<String> urlPageList = new ArrayList<String>();
	private ArrayList<HashMap<String, String>> scrappedPlayerStatsList = new ArrayList<HashMap<String, String>>();

	private boolean odd;
	private boolean even;
	private ArrayList<String> statGrp;
	private int count;
	private final int POS = 1;
	private final int NUM = 2;
	private final int PLAYER_NAME = 3;
	private final int STATUS = 4;
	private final int TCKL = 8;
	private final int SCK = 12;
	private final int INTT = 20;
	private final int TEAM = 21;

	private final int FIRST_PAGE = 1;
	private final int LAST_PAGE = 7;

	// constructor
	public Scraper(String url) {
		this.url = url;
	}// end method

	// reads the data from a web page and searches for the string matches
	public void parseData() throws Exception {
		setUrlPages();
		for (int i = 0; i < urlPageList.size() - 1; i++) {
			String urlString = urlPageList.get(i);

			// create the url
			URL url = new URL(urlString);

			// open the url stream, wrap it an a few "readers"
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				processGrpPatternMatch(line);
			}
			playerStatsList.add(statGrp);
			processIndNFLStatPatternMatch();
		}
	}

	private void setUrlPages() throws Exception {
		String urlRegxPattern = "(\"\\/players\\/search\\?)(.*?)(filter=defensiveback\")";
		Pattern urlPattern = Pattern.compile(urlRegxPattern, Pattern.CASE_INSENSITIVE);

		// create the url
		URL url = new URL(this.url);

		// open the url stream, wrap it an a few "readers"
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

		String line;
		boolean matchFound = false;
		urlPageList.add(this.url);
		while ((line = reader.readLine()) != null) {
			Matcher urlMatcher = urlPattern.matcher(line);
			while (urlMatcher.find()) {
				String nextUrl = line.substring(urlMatcher.start(), urlMatcher.end());
				String clean1Url = nextUrl.replace("\"", "");
				String clean2Url = clean1Url.replace("&amp;", "&");
				String clean3Url = clean2Url.replace(" ", "%20");
				urlPageList.add("http://www.nfl.com" + clean3Url);
				matchFound = true;
			}
			if (matchFound) {
				break;
			}
		}

	}
	
	private void processGrpPatternMatch(String line) {
		String oddTrRegxPattern = "(<tr class=\"odd\">)";
		String evenTrRegxPattern = "(<tr class=\"even\">)";

		Pattern oddPlayerPattern = Pattern.compile(oddTrRegxPattern, Pattern.CASE_INSENSITIVE);
		Matcher oddPlayerMatcher = oddPlayerPattern.matcher(line);

		Pattern evenPlayerPattern = Pattern.compile(evenTrRegxPattern, Pattern.CASE_INSENSITIVE);
		Matcher evenPlayerMatcher = evenPlayerPattern.matcher(line);

		if (oddPlayerMatcher.find()) {
			if (count > 0) {
				playerStatsList.add(statGrp);
			}
			String oddTagLine = line.substring(oddPlayerMatcher.start(), oddPlayerMatcher.end());
			statGrp = new ArrayList<String>();
			statGrp.add(oddTagLine);
			odd = true;
			even = false;
		} else if (evenPlayerMatcher.find()) {
			count++;
			if (count > 0) {
				playerStatsList.add(statGrp);
			}
			String evenTagLine = line.substring(evenPlayerMatcher.start(), evenPlayerMatcher.end());
			statGrp = new ArrayList<String>();
			statGrp.add(evenTagLine);
			even = true;
			odd = true;
		} else {
			if (odd || even) {
				statGrp.add(line);
			}
		}
	}

	private void processIndNFLStatPatternMatch() {

		String posRegxPattern = "(<td class=\"tbdy\">)(.*?)(</td>)";
		String numRegxPattern = "(<td class=\"tbdy\">)(.*?)(</td>)";
		String playerNameRegxPattern = "(/profile\">)(.*?)(</a></td>)";
		String statusRegxPattern = "(<td class=\"tbdy\">)(.*?)(</td>)";
		String tcklRegxPattern = "(<td class=\"tbdy\">)(.*?)(</td>)";
		String sckRegxPattern = "(<td class=\"tbdy\">)(.*?)(</td>)";
		String inttRegxPattern = "(<td class=\"tbdy\">)(.*?)(</td>)";
		String teamRegxPattern = "(/profile\\?team=)(.*?)(\")";

		Pattern pos = Pattern.compile(posRegxPattern, Pattern.CASE_INSENSITIVE);
		Pattern num = Pattern.compile(numRegxPattern, Pattern.CASE_INSENSITIVE);
		Pattern playerName = Pattern.compile(playerNameRegxPattern, Pattern.CASE_INSENSITIVE);
		Pattern status = Pattern.compile(statusRegxPattern, Pattern.CASE_INSENSITIVE);
		Pattern tckl = Pattern.compile(tcklRegxPattern, Pattern.CASE_INSENSITIVE);
		Pattern sck = Pattern.compile(sckRegxPattern, Pattern.CASE_INSENSITIVE);
		Pattern intt = Pattern.compile(inttRegxPattern, Pattern.CASE_INSENSITIVE);
		Pattern team = Pattern.compile(teamRegxPattern, Pattern.CASE_INSENSITIVE);

		Regex regex = new Regex(pos, num, playerName, status, tckl, sck, intt, team);

		HashMap<String, String> scrappedHash = null;

		for (int i = 0; i < playerStatsList.size(); i++) {
			ArrayList<String> playerStats = playerStatsList.get(i);
			scrappedHash = new HashMap<>();
			for (int b = 0; b < playerStats.size(); b++) {
				String line = playerStats.get(b);
				switch (b) {
				case POS:
					Matcher posMatcher = regex.getPos().matcher(line);
					if (posMatcher.find()) {
						String tagLine = line.substring(posMatcher.start(), posMatcher.end());
						String posTxt = tagLine.replace("<td class=\"tbdy\">", "");
						scrappedHash.put("POS", posTxt.replace("</td>", ""));
						if (scrappedHash.get("POS").length() == 2) {
							scrappedHash.put("POS", posTxt.replace("</td>", "  "));
						}
						if (scrappedHash.get("POS").length() == 3) {
							scrappedHash.put("POS", posTxt.replace("</td>", " "));
						}
					}
					break;
				case NUM:
					Matcher numMatcher = regex.getNum().matcher(line);
					if (numMatcher.find()) {
						String tagLine = line.substring(numMatcher.start(), numMatcher.end());
						String numTxt = tagLine.replace("<td class=\"tbdy\">", "");
						scrappedHash.put("NUM", numTxt.replace("</td>", ""));
						if (scrappedHash.get("NUM").length() == 0) {
							scrappedHash.put("NUM", numTxt.replace("</td>", "--"));
						}
						if (scrappedHash.get("NUM").length() == 1) {
							scrappedHash.put("NUM", numTxt.replace("</td>", "-"));
						}
					}
					break;
				case PLAYER_NAME:
					Matcher playerNameMatcher = regex.getPlayerName().matcher(line);
					if (playerNameMatcher.find()) {
						String tagLine = line.substring(playerNameMatcher.start(), playerNameMatcher.end());
						String playerNameTxt = tagLine.replace("/profile\">", "");
						scrappedHash.put("PLAYER_NAME", playerNameTxt.replace("</a></td>", ""));
						int x = (35 - scrappedHash.get("PLAYER_NAME").length());
						if (scrappedHash.get("PLAYER_NAME").length() <= 35) {
							String y = "";
								for (int j = x ; j > 0; j--){
									y += " ";
								}
							scrappedHash.put("PLAYER_NAME", playerNameTxt.replace("</a></td>", y));
						}
					}
					break;
				case STATUS:
					Matcher statusMatcher = regex.getStatus().matcher(line);
					if (statusMatcher.find()) {
						String tagLine = line.substring(statusMatcher.start(), statusMatcher.end());
						String statusTxt = tagLine.replace("<td class=\"tbdy\">", "");
						scrappedHash.put("STATUS", statusTxt.replace("</td>", ""));
					}
					break;
				case TCKL:
					Matcher tcklMatcher = regex.getTckl().matcher(line);
					if (tcklMatcher.find()) {
						String tagLine = line.substring(tcklMatcher.start(), tcklMatcher.end());
						String tcklTxt = tagLine.replace("<td class=\"tbdy\">", "");
						scrappedHash.put("TCKL", tcklTxt.replace("</td>", ""));
						if (scrappedHash.get("TCKL").length() == 1) {
							scrappedHash.put("TCKL", tcklTxt.replace("</td>", " "));
						}
					}
					
					break;
				case SCK:
					Matcher sckMatcher = regex.getSck().matcher(line);
					if (sckMatcher.find()) {
						String tagLine = line.substring(sckMatcher.start(), sckMatcher.end());
						String sckTxt = tagLine.replace("<td class=\"tbdy\">", "");
						scrappedHash.put("SCK", sckTxt.replace("</td>", ""));
						if (scrappedHash.get("SCK").length() == 2) {
							scrappedHash.put("SCK", sckTxt.replace("</td>", " "));
						}
					}
					break;
				case INTT:
					Matcher intMatcher = regex.getIntt().matcher(line);
					if (intMatcher.find()) {
						String tagLine = line.substring(intMatcher.start(), intMatcher.end());
						String inttTxt = tagLine.replace("<td class=\"tbdy\">", "");
						scrappedHash.put("INTT", inttTxt.replace("</td>", ""));
						if (scrappedHash.get("INTT").length() == 1) {
							scrappedHash.put("INTT", inttTxt.replace("</td>", "  "));
						}
					}
					break;
				case TEAM:
					Matcher teamMatcher = regex.getTeam().matcher(line);
					if (teamMatcher.find()) {
						String tagLine = line.substring(teamMatcher.start(), teamMatcher.end());
						String teamTxt = tagLine.replace("/profile?team=", "");
						scrappedHash.put("TEAM", teamTxt.replace("\"", ""));
					}
					break;
				}
			}
			scrappedPlayerStatsList.add(scrappedHash);
		}
		playerStatsList = new ArrayList<ArrayList<String>>();
	}

	// shows the output (scraped data) in a text-area
	public ArrayList<HashMap<String, String>> display() {
		return scrappedPlayerStatsList;
	}

} // end class
